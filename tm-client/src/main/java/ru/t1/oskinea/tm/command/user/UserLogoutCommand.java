package ru.t1.oskinea.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.user.UserLogoutRequest;
import ru.t1.oskinea.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Logout current user.";

    @NotNull
    private static final String NAME = "logout";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());

        getAuthEndpoint().logout(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
