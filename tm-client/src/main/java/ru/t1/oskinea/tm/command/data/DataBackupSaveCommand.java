package ru.t1.oskinea.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.data.DataBackupSaveRequest;
import ru.t1.oskinea.tm.enumerated.Role;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data to backup file.";

    @NotNull
    public static final String NAME = "backup-save";

    @Override
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(getToken());
        getDomainEndpoint().saveDataBackup(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
