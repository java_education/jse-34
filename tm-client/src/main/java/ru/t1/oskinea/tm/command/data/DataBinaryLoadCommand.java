package ru.t1.oskinea.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.data.DataBinaryLoadRequest;
import ru.t1.oskinea.tm.enumerated.Role;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from binary file.";

    @NotNull
    public static final String NAME = "data-load-binary";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        getDomainEndpoint().loadDataBinary(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
