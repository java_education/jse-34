package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.task.TaskGetByIdRequest;
import ru.t1.oskinea.tm.dto.response.task.TaskGetByIdResponse;
import ru.t1.oskinea.tm.model.Task;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Show task by id.";

    @NotNull
    private static final String NAME = "task-show-by-id";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);

        @NotNull TaskGetByIdResponse response = getTaskEndpoint().getTaskById(request);
        @Nullable Task task = response.getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
