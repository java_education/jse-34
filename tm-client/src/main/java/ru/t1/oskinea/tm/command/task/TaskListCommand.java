package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.task.TaskListRequest;
import ru.t1.oskinea.tm.dto.response.task.TaskListResponse;
import ru.t1.oskinea.tm.enumerated.Sort;
import ru.t1.oskinea.tm.model.Task;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Show task list.";

    @NotNull
    private static final String NAME = "task-list";

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.print("ENTER SORT (");
        System.out.print(Arrays.toString(Sort.values()));
        System.out.print("): ");
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        if (sort != null) request.setSort(sort);
        @NotNull TaskListResponse response = getTaskEndpoint().listTask(request);

        @NotNull final List<Task> tasks = response.getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
