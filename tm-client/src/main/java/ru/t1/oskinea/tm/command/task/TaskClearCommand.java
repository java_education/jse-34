package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.task.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Delete all tasks.";

    @NotNull
    private static final String NAME = "task-clear";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");

        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());

        getTaskEndpoint().clearTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
