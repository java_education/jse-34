package ru.t1.oskinea.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.model.ICommand;
import ru.t1.oskinea.tm.api.service.IPropertyService;
import ru.t1.oskinea.tm.api.service.IServiceLocator;
import ru.t1.oskinea.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Getter
    @Setter
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += (!result.isEmpty() ? ", " : "") + argument;
        if (!description.isEmpty()) result += (!result.isEmpty() ? " - " : "") + description;
        return result;
    }

}
