package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    void setToken(@Nullable String token);

    @Nullable
    String getToken();

}
