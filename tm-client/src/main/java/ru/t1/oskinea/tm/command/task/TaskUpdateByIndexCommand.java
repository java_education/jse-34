package ru.t1.oskinea.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Update task by index.";

    @NotNull
    private static final String NAME = "task-update-by-index";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);

        getTaskEndpoint().updateTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
