package ru.t1.oskinea.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.oskinea.tm.dto.response.project.ProjectGetByIdResponse;
import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Show project by id.";

    @NotNull
    private static final String NAME = "project-show-by-id";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setId(id);

        @NotNull ProjectGetByIdResponse response = getProjectEndpoint().getProjectById(request);
        @Nullable Project project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
