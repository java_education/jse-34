package ru.t1.oskinea.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;
import ru.t1.oskinea.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIdResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public ProjectGetByIdResponse(@Nullable final Project project) {
        this.project = project;
    }

}
