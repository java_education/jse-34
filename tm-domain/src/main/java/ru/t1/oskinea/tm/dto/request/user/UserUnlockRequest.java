package ru.t1.oskinea.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserUnlockRequest extends AbstractUserRequest {

    @NotNull
    private String login;

    public UserUnlockRequest(@Nullable final String token) {
        super(token);
    }

}
