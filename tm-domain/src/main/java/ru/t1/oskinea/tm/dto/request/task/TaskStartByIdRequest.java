package ru.t1.oskinea.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskStartByIdRequest extends AbstractUserRequest {

    @NotNull
    private String id;

    public TaskStartByIdRequest(@Nullable final String token) {
        super(token);
    }

}
