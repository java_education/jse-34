package ru.t1.oskinea.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

public final class DataJsonLoadJaxBRequest extends AbstractUserRequest {

    public DataJsonLoadJaxBRequest(@Nullable final String token) {
        super(token);
    }

}
