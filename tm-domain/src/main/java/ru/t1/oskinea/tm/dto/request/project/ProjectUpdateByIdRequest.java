package ru.t1.oskinea.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIdRequest extends AbstractUserRequest {

    @NotNull
    private String id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    public ProjectUpdateByIdRequest(@Nullable final String token) {
        super(token);
    }

}
