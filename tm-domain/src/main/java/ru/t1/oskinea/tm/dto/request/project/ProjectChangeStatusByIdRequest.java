package ru.t1.oskinea.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;
import ru.t1.oskinea.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @NotNull
    private String id;

    @NotNull
    private Status status;

    public ProjectChangeStatusByIdRequest(@NotNull final String id, @NotNull final Status status) {
        this.id = id;
        this.status = status;
    }

    public ProjectChangeStatusByIdRequest(@Nullable final String token) {
        super(token);
    }

}
