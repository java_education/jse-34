package ru.t1.oskinea.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    public ProjectStartByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
