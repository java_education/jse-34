package ru.t1.oskinea.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

public final class DataJsonLoadFasterXmlRequest extends AbstractUserRequest {

    public DataJsonLoadFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
