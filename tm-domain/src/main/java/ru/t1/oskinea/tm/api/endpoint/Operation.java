package ru.t1.oskinea.tm.api.endpoint;

import ru.t1.oskinea.tm.dto.request.AbstractRequest;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
