package ru.t1.oskinea.tm.dto.response.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class DataBinaryLoadResponse extends AbstractResponse {
}
