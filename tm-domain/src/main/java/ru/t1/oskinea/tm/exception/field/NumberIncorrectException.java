package ru.t1.oskinea.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class NumberIncorrectException extends AbstractFieldException {

    @SuppressWarnings("unused")
    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(
            @NotNull final String value,
            @SuppressWarnings("unused") @Nullable Throwable cause
    ) {
        super("Error! This value \"" + value + "\" is incorrect...");
    }

}
