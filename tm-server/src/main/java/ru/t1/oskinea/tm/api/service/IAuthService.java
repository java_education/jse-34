package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.model.Session;
import ru.t1.oskinea.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@NotNull String login, @NotNull String password);

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session);

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

}
