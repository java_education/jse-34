package ru.t1.oskinea.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.api.endpoint.ISystemEndpoint;
import ru.t1.oskinea.tm.api.service.IPropertyService;
import ru.t1.oskinea.tm.api.service.IServiceLocator;
import ru.t1.oskinea.tm.dto.request.system.ServerAboutRequest;
import ru.t1.oskinea.tm.dto.request.system.ServerVersionRequest;
import ru.t1.oskinea.tm.dto.response.system.ServerAboutResponse;
import ru.t1.oskinea.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.oskinea.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        response.setApplicationName(propertyService.getApplicationName());
        response.setGitBranch(propertyService.getGitBranch());
        response.setGitCommitId(propertyService.getGitCommitId());
        response.setGitCommitterName(propertyService.getGitCommitterName());
        response.setGitCommitterEmail(propertyService.getGitCommitterEmail());
        response.setGitCommitMessage(propertyService.getGitCommitMessage());
        response.setGitCommitTime(propertyService.getGitCommitTime());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
