package ru.t1.oskinea.tm.repository;

import ru.t1.oskinea.tm.api.repository.ISessionRepository;
import ru.t1.oskinea.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
