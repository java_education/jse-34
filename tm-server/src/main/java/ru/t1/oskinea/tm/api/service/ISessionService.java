package ru.t1.oskinea.tm.api.service;

import ru.t1.oskinea.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
