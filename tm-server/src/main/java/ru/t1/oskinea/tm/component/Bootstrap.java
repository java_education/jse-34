package ru.t1.oskinea.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.api.endpoint.*;
import ru.t1.oskinea.tm.api.repository.IProjectRepository;
import ru.t1.oskinea.tm.api.repository.ISessionRepository;
import ru.t1.oskinea.tm.api.repository.ITaskRepository;
import ru.t1.oskinea.tm.api.repository.IUserRepository;
import ru.t1.oskinea.tm.api.service.*;
import ru.t1.oskinea.tm.endpoint.*;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.model.User;
import ru.t1.oskinea.tm.repository.ProjectRepository;
import ru.t1.oskinea.tm.repository.SessionRepository;
import ru.t1.oskinea.tm.repository.TaskRepository;
import ru.t1.oskinea.tm.repository.UserRepository;
import ru.t1.oskinea.tm.service.*;
import ru.t1.oskinea.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @Getter
    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getHost();
        @NotNull final String port = getPropertyService().getPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void exit() {
        System.exit(0);
    }

    private void initBackup() {
        backup.start();
    }

    private void initData() {
        initBackup();
        final boolean checkBackup = Files.exists(Paths.get(DomainService.FILE_BACKUP));
        if (checkBackup) return;
        final boolean checkBinary = Files.exists(Paths.get(DomainService.FILE_BINARY));
        if (checkBinary) getDomainService().loadDataBinary();
        if (checkBinary) return;
        final boolean checkBase64 = Files.exists(Paths.get(DomainService.FILE_BASE64));
        if (checkBase64) getDomainService().loadDataBase64();
        if (checkBase64) return;
        initDemoData();
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@comp.ru");
        @NotNull final User user = userService.create("user", "user", "user@comp.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(test.getId(), "project 1", "d1");
        projectService.create(test.getId(), "project 2", "d2");
        projectService.create(test.getId(), "project 3", "d3");
        projectService.create(test.getId(), "project 4", "d4");
        taskService.create(test.getId(), "task 1", "d1");
        taskService.create(test.getId(), "task 2", "d2");

        projectService.create(user.getId(), "user project 1", "ud1");
        taskService.create(user.getId(), "user task 1", "ut1");

        projectService.create(admin.getId(), "admin project 1", "ad1");
        taskService.create(admin.getId(), "admin task 1", "at1");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER (server) **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processOnShutdown() {
        backup.stop();

        loggerService.info("*** TASK MANAGER IS SHUTTING DOWN (server) ***");
    }

    public void start() {
        initPID();
        initData();
        initLogger();

        Runtime.getRuntime().addShutdownHook(new Thread(this::processOnShutdown));
    }

}
