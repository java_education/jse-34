package ru.t1.oskinea.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.endpoint.IAuthEndpoint;
import ru.t1.oskinea.tm.api.service.IAuthService;
import ru.t1.oskinea.tm.api.service.IServiceLocator;
import ru.t1.oskinea.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLoginRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLogoutRequest;
import ru.t1.oskinea.tm.dto.response.user.UserGetProfileResponse;
import ru.t1.oskinea.tm.dto.response.user.UserLoginResponse;
import ru.t1.oskinea.tm.dto.response.user.UserLogoutResponse;
import ru.t1.oskinea.tm.model.Session;
import ru.t1.oskinea.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.oskinea.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @Nullable final Session session = check(request);
        getServiceLocator().getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserGetProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserGetProfileRequest request
    ) {
        @Nullable final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        return new UserGetProfileResponse(user);
    }

}
