package ru.t1.oskinea.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.api.repository.ISessionRepository;
import ru.t1.oskinea.tm.api.service.ISessionService;
import ru.t1.oskinea.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }
}
