package ru.t1.oskinea.tm.api.repository;

import ru.t1.oskinea.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
